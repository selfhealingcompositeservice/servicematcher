package fr.dauphine.test;

import fr.dauphine.matcher.ontology.ConceptMatcher;

public class ConceptMatcherTest {

	public static void main(String[] args) {
		ConceptMatcher conceptMathcher = ConceptMatcher.getInstance();
		
		System.out.println("Equivalents: ");
		System.out.println("Score(Activity,Activity): " + conceptMathcher.getScore("Activity", "Activity"));
		
		System.out.println("Plugins: ");
		System.out.println("Score(Activity,Surfing): " + conceptMathcher.getScore("ACTIVITY", "SURFING"));
		System.out.println("Score(Activity,Sports): " + conceptMathcher.getScore("Activity", "sports"));
		System.out.println("Score(Sports,Surfing): " + conceptMathcher.getScore("Sports", "SURFING"));
		
		System.out.println("Subsumptions: ");
		System.out.println("Score(Surfing,Activity): " + conceptMathcher.getScore("SURFING", "ACTIVITY"));
		System.out.println("Score(Sports,Activity): " + conceptMathcher.getScore("Sports", "AcTivity"));
		System.out.println("Score(Surfing,Sports): " + conceptMathcher.getScore("SURFING", "Sports"));
	}

}