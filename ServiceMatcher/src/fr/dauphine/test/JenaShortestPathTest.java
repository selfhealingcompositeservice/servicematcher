package fr.dauphine.test;

import fr.dauphine.matcher.ontology.JenaShortestPath;



public class JenaShortestPathTest {

	static String ontologyLocation = "http://localhost/ontology/travel.owl";
	static String activity = "http://127.0.0.1/ontology/travel.owl#Activity";
	static String surfing = "http://127.0.0.1/ontology/travel.owl#Surfing";
	static String sports = "http://127.0.0.1/ontology/travel.owl#Sports";
	
	public static void main(String[] args) {
		
		int d;
		
		d = JenaShortestPath.findJenaDistance(ontologyLocation, surfing, activity);
		assert(d == 2);
		
		d = JenaShortestPath.findJenaDistance(ontologyLocation, sports, activity);
		assert(d == 1);
		
		d = JenaShortestPath.findJenaDistance(ontologyLocation, sports, sports);
		assert(d == 0);
		
		d = JenaShortestPath.findJenaDistance(ontologyLocation, surfing, sports);
		assert(d == 1);
		
		System.out.println("Test finished successfully");
	}

}
