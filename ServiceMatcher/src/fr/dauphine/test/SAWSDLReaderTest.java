package fr.dauphine.test;

import fr.dauphine.matcher.wsdl.SAWSDLReader;
import fr.dauphine.matcher.wsdl.WSDLReader;

public class SAWSDLReaderTest {
	
	public static void main(String...args) {
		WSDLReader reader = new SAWSDLReader("/var/www/services/sawsdl_wsdl11/author_bookprice_service.wsdl");
		System.out.println("Inputs: " + reader.getInputs());
		System.out.println("Outpputs: " + reader.getOutputs());
	}

}
