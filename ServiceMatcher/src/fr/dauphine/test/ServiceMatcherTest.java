package fr.dauphine.test;

import java.util.ArrayList;
import java.util.List;

import fr.dauphine.matcher.ServiceMatcher;
import fr.dauphine.query.Query;
import fr.dauphine.utils.Utils;

public class ServiceMatcherTest {

	public static void main(String[] args) {
		System.out.println("Service Matcher");
		ServiceMatcher matcher = new ServiceMatcher("/var/www/services/sawsdl_wsdl11/author_bookprice_service.wsdl",
				"/var/www/services/sawsdl_wsdl11/author_bookprice_service.wsdl");
		System.out.println("score: " + matcher.getFunctionalSimilarity());
		
		System.out.println("Query Matcher");
		Query query = new Query();
		List<String> inputs = new ArrayList<>();
		inputs.add("AUTHOR");
		List<String> outputs = new ArrayList<>();
		outputs.add("BOOK");
		outputs.add("PRICE");
		query.setInputs(Utils.asDataList(inputs));
		query.setOutputs(Utils.asDataList(outputs));
		matcher = new ServiceMatcher(query,
				"/var/www/services/sawsdl_wsdl11/author_bookprice_service.wsdl");
		System.out.println("score: " + matcher.getFunctionalSimilarity());
	}

}
