package fr.dauphine.matcher.ontology;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

public class ConceptMatcher {
	
	String ontologyLocation = "http://localhost/ontology/travel.owl";
	String ontology2location = "http://127.0.0.1/ontology/portal.owl";
	private static ConceptMatcher instance = null;
	private OWLOntologyManager manager;
	private OWLOntology ontology, ontology2;
	private PelletReasoner reasoner;
	private Map<String, OWLClass> ontologyClasses;
	
	Set<OWLOntology> owlOntologies = null;
	
	 public static ConceptMatcher getInstance() {
        if (instance == null) {
            instance = new ConceptMatcher();
        }
        return instance;
    }
	 
	private ConceptMatcher() {
		try {
			initialise();
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
	}
	
	private void initialise() throws OWLOntologyCreationException {
		manager = OWLManager.createOWLOntologyManager();
		ontology = manager.loadOntology(IRI.create(ontologyLocation));
		ontology2 = manager.loadOntology(IRI.create(ontology2location));
		reasoner = PelletReasonerFactory.getInstance().createReasoner(ontology);
		reasoner.getKB().realize();
		//reasoner.getKB().printClassTree();
		
		owlOntologies = new HashSet<>();
		owlOntologies.add(ontology);
		owlOntologies.add(ontology2);
		
		//load classes
		ontologyClasses = new HashMap<String, OWLClass>();
		for(OWLClass c:ontology.getClassesInSignature())
			ontologyClasses.put(c.getIRI().getFragment().toLowerCase(), c);
	}
	
	public float getScore(String baseClass, String evaluatedClass) {
		
		OWLClass class1 = getClass(baseClass);
		OWLClass class2 = getClass(evaluatedClass);
		
		if(class1 == null || class2 == null)
			return 0.0f;

		ConceptMatcherResult match = match(class1, class2);
		
		//optimizar la carga de subclases y super clases, ya que 
		//esto ya se hizo al calcular subsumes y plugin
		double ontologicalDistance = getOntologicalDistance(class1,class2, match);
		
		return (float) (matchingDegree(match)*ontologicalDistance);
		
	}
	
	//ontological distance of evaluatedClass respect to baseClass
	private double getOntologicalDistance(OWLClass baseClass, OWLClass evaluatedClass, ConceptMatcherResult match) {
		int ontologicalDistance = 0;
		int max = 0, min = 0;
		switch (match) {
	        case Exact:
	            return 1.0;
	        case Subsumption:
	        	ontologicalDistance = getDistance(baseClass, evaluatedClass);
	        	//TODO: como calcular el maximo???? :(
	        	max = 4;
	        	break;
	        case PlugIn:
	        	ontologicalDistance = getDistance(evaluatedClass, baseClass);
	        	//TODO: como calcular el maximo???? :(
	        	max = 4;
	        	break;
	        case NotMatched:
	            return 0.0;
		}
		
		double normalized = (double)(ontologicalDistance - max)/(min - max);
		//double scaled = (0.1 - 1)*normalized + 1;
		//System.out.println("ontologicalDistance: " + normalized);
		return  normalized;
		
	}
	



	private int getDistance(OWLClass evaluatedClass, OWLClass baseClass) {
		
		//optimizar
		
		
		
		return JenaShortestPath.findJenaDistance(ontologyLocation, 
				evaluatedClass.getIRI().toURI().toString(), baseClass.getIRI().toURI().toString());
	}

	private OWLClass getClass(String name) {
		
		return ontologyClasses.get(name.toLowerCase());
	}
	
	
	private ConceptMatcherResult match(OWLClass c1, OWLClass c2) {
		if (isEquivalent(c1, c2)) {
            return ConceptMatcherResult.Exact;
        } else if (subsumes(c1, c2)) {
            return ConceptMatcherResult.Subsumption;
        } else if (isPlugin(c1, c2)) {
            return ConceptMatcherResult.PlugIn;
        }

        return ConceptMatcherResult.NotMatched;
	}

	private float matchingDegree(ConceptMatcherResult match) {
        switch (match) {
            case Exact:
                return 1.0f;
            case Subsumption:
                return 0.8f;
            case PlugIn:
                return 0.8f;
            case NotMatched:
                return 0.0f;
            default:
                return 0.0f;
        }
    }
	
	//Should be part of a reasoner?
	boolean isEquivalent(OWLClass c1, OWLClass c2) {
    	boolean equivalent = false;
    	Set<OWLClassExpression> equivalentclasses = c2.getEquivalentClasses(owlOntologies);
    	equivalent = equivalentclasses.contains(c1) || c1.equals(c2);
    	
    	return equivalent;
    }
    
    //c1 subsumes c2 <-> c2 is subclass of c1
    boolean subsumes(OWLClass baseClass, OWLClass evaluatedClass) {
    	boolean subsumes = false;
    	//Set<OWLClassExpression> superclasses = c2.getSuperClasses(ontology);
    	NodeSet<OWLClass> subClasses = reasoner.getSubClasses(evaluatedClass, false);
    	//subsumes = superclasses.contains(c1);
    	subsumes = subClasses.containsEntity(baseClass);
    	
    	return subsumes;
    }
    
    //c1 is plugins of c2 <-> c1 is subclass of c2
    boolean isPlugin(OWLClass baseClass, OWLClass evaluatedClass) {
    	boolean plugin = false;
    	//Set<OWLClassExpression> subClasses = c2.getSubClasses(ontology);
    	//plugin = subClasses.contains(c1);
    	NodeSet<OWLClass> superclasses = reasoner.getSuperClasses(evaluatedClass, false);
    	plugin = superclasses.containsEntity(baseClass);
    	
    	return plugin;
    }
}
