package fr.dauphine.matcher.ontology;

public enum ConceptMatcherResult {

    Exact,
    PlugIn,
    Subsumption,
    NotMatched
}
