package fr.dauphine.matcher.ontology;

import java.io.InputStream;
import java.util.Iterator;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntTools;
import com.hp.hpl.jena.ontology.OntTools.Path;
import com.hp.hpl.jena.ontology.OntTools.PredicatesFilter;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.util.iterator.Filter;
import com.hp.hpl.jena.vocabulary.RDFS;

public class JenaShortestPath {

	public static int findJenaDistance(String ontologyLocation, String fromSubClassName, String toSuperClassName) {

        // Jena implementation 

        long startTime = System.currentTimeMillis();
        

        OntModel model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
        InputStream in = FileManager.get().open(ontologyLocation);
        model.read(in, "");        
        
        //System.out.format("Ontology load time: (%7.2f sec)%n%n", (System.currentTimeMillis() - startTime) / 1000.0);        
     


		/*Iterator<OntClass> it = model.listClasses();
		while (it.hasNext()) {
		  OntClass class = it.next();
		  System.out.println(class.getLabel(null));
		}*/
        OntClass fromSubClass = model.getOntClass(fromSubClassName);        
        OntClass toSuperClass = model.getOntClass(toSuperClassName);

        
        //Filter<Statement> filter = OntTools.PredicatesFilter(Filter.any)
        Path path = OntTools.findShortestPath(model, fromSubClass, toSuperClass, new Filter<Statement>() {

			@Override
			public boolean accept(Statement arg0) {
				//System.out.println(arg0.getPredicate().equals(RDFS.subClassOf));
				return arg0.getPredicate().equals(RDFS.subClassOf);
			}
		});
        int superClasses = 0;
        if (path != null){
            
            for (Statement s: path) {
                //if (s.getObject().toString().startsWith(ns)) {
                    // filter out OWL Classes
                    superClasses++;
                    //System.out.println(s.getObject());
                //}
            }
            //System.out.println("Shortest distance from " + fromSubClass + " to " + toSuperClass + " = " + superClasses);
        }else if (fromSubClass == toSuperClass){
            //System.out.println("Same node");
        }else {
            //System.out.println("No path from " + fromSubClass + " to " + toSuperClass);
        }   

       // System.out.format("\nProcessing time: (%7.2f sec)%n%n", (System.currentTimeMillis() - startTime) / 1000.0);
        return superClasses;
    }
}
