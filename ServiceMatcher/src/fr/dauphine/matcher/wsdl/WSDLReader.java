package fr.dauphine.matcher.wsdl;

import java.util.List;

public interface WSDLReader {
	
	String getServiceName();
	List<String> getInputs();
	List<String> getOutputs();

}
