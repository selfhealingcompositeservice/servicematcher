package fr.dauphine.matcher.wsdl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.wsdl.WSDLException;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;
import javax.xml.namespace.QName;

import com.ibm.wsdl.Constants;

import edu.uga.cs.lsdis.sawsdl.Definition;
import edu.uga.cs.lsdis.sawsdl.Message;
import edu.uga.cs.lsdis.sawsdl.Operation;
import edu.uga.cs.lsdis.sawsdl.Part;
import edu.uga.cs.lsdis.sawsdl.PortType;

public class SAWSDLReader implements fr.dauphine.matcher.wsdl.WSDLReader {
	
	Definition definition;
	List<String> inputs;
	List<String> outputs;
	
	public SAWSDLReader(String file) {
		System.setProperty("javax.wsdl.factory.WSDLFactory",
                "edu.uga.cs.lsdis.sawsdl.impl.factory.WSDLFactoryImpl");
		inputs = new ArrayList<>();
		outputs = new ArrayList<>();

        try {
			WSDLReader wsdlReader = WSDLFactory.newInstance().newWSDLReader();
			wsdlReader.setFeature(Constants.FEATURE_VERBOSE, false);
			String wsdlURI = new File(file).toURI().toString();
			definition = (Definition) wsdlReader.readWSDL(wsdlURI);
			read();
		} catch (WSDLException e) {
			e.printStackTrace();
		}
	}

	private void read() {
         
         for (Object key:definition.getPortTypes().keySet()){
             PortType semanticPortType = definition.getSemanticPortType((QName)key);

             for (Object operation : semanticPortType.getOperations()) {
                 
                 Message semanticMessage = definition.getSemanticMessage(((Operation) operation).getInput().getMessage().getQName());

                 for (Object partKey : semanticMessage.getParts().keySet()) {
                     Part semanticPart = semanticMessage.getSemanticPart((String) partKey);
                     inputs.add(parseString(semanticPart.getName()));
                 }
                 
                 semanticMessage = definition.getSemanticMessage(((Operation) operation).getOutput().getMessage().getQName());

                 for (Object partKey : semanticMessage.getParts().keySet()) {
                     Part semanticPart = semanticMessage.getSemanticPart((String) partKey);
                     outputs.add(parseString(semanticPart.getName()));
                 }
             }
         }
	}

	@Override
	public List<String> getInputs() {
		return inputs;
	}

	@Override
	public List<String> getOutputs() {
		return outputs;
	}
	
	

	@Override
	public String getServiceName() {
		
		return definition.getQName().getLocalPart();
	}

	//I dont know why this happens T.T
	private String parseString(String s) {
		if(s.charAt(0) == '_')
			return s.substring(1, s.length());
		return s;
	}
}
