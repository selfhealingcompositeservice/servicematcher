package fr.dauphine.matcher;

import java.util.List;

import fr.dauphine.matcher.ontology.ConceptMatcher;
import fr.dauphine.matcher.wsdl.SAWSDLReader;
import fr.dauphine.matcher.wsdl.WSDLReader;
import fr.dauphine.query.Query;
import fr.dauphine.utils.Utils;

public class ServiceMatcher {
	
	String serviceName;
	String candidateName;
	Query query;
	List<String> inputs = null;
	List<String> outputs = null;
	
	public ServiceMatcher(String serviceName, String candidateName) {
		super();
		this.serviceName = serviceName;
		this.candidateName = candidateName;
		WSDLReader readerService = new SAWSDLReader(serviceName);
		inputs = readerService.getInputs();
		outputs = readerService.getOutputs();
	}
	
	public ServiceMatcher(Query query, String candidateName) {
		super();
		this.query = query;
		this.candidateName = candidateName;
		inputs = Utils.asStringList(query.getInputs());
		outputs = Utils.asStringList(query.getOutputs());
	}
	
	public double getFunctionalSimilarity() {
		double similarity = 0.0;
		
		WSDLReader readerCandidate = new SAWSDLReader(candidateName);
		
		if(readerCandidate.getInputs().size() > inputs.size()
				||
		   readerCandidate.getOutputs().size() < outputs.size())
			return 0.0;
		
		similarity = score(readerCandidate.getInputs(), inputs)
				*score(outputs, readerCandidate.getOutputs());
		
		
		return similarity;
	}
	

	//returns the product of the best similarity score of each concept in 
	//"concepts1" respect to all concepts in "concepts2"
	private double score(List<String> concepts1, List<String> concepts2) {
		double totalScore = 1.0;
		for(String c1:concepts1) {
			double conceptScore = 0.0;
			for(String c2:concepts2) {
				ConceptMatcher conceptMatcher = ConceptMatcher.getInstance();
				float score = conceptMatcher.getScore(c1, c2);
				if(score > conceptScore)
					conceptScore = score;
				//best possible score, so we stop comparing with "concepts2"
				if(score == 1.0) 
					break;
			}
			//c1 does not match any concept in "concept2", so there is nothing else to do
			if(conceptScore == 0.0)
				return 0.0;
			totalScore *= conceptScore;
		}
		return totalScore;
	}

}
