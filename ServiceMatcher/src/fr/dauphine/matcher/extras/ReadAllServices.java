package fr.dauphine.matcher.extras;

import java.io.File;
import java.util.Scanner;

import fr.dauphine.matcher.wsdl.SAWSDLReader;
import fr.dauphine.matcher.wsdl.WSDLReader;

public class ReadAllServices {

	final static String wsdlFolder = "/var/www/domains/travel";
	
	public void readAllServices() {
		Scanner scanner = new Scanner(System.in);

        File folder = new File(wsdlFolder);
        File[] listOfFiles = folder.listFiles();

        System.out.println("Travel Web Services:");
        for (int i = 0; i < listOfFiles.length; i++)
            if (listOfFiles[i].isFile()) {
                String file_name;
                file_name = listOfFiles[i].getName();
                System.out.println("    " + i + " - " + file_name);
            }
        
        System.out.println("********************************");
        //evaluate each WS in the repository
        for (int i = 0; i < listOfFiles.length; i++)
            if (listOfFiles[i].isFile()) {
            	String serviceExaminee = listOfFiles[i].getAbsolutePath();
            	
        		WSDLReader examineeService = new SAWSDLReader(serviceExaminee);
                System.out.println("name: " + examineeService.getServiceName());
                System.out.println("inputs: " + examineeService.getInputs());
                System.out.println("outputs: " + examineeService.getOutputs());
                System.out.println("********************************");
            }
        	
        
	}
	
	public static void main(String...args) {
		ReadAllServices reader = new ReadAllServices();
		reader.readAllServices();
	}
	
	
}
