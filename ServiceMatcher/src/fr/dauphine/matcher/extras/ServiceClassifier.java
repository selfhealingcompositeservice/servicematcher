package fr.dauphine.matcher.extras;

import java.io.File;
import java.util.Scanner;

import fr.dauphine.matcher.ServiceMatcher;
import fr.dauphine.matcher.wsdl.SAWSDLReader;
import fr.dauphine.matcher.wsdl.WSDLReader;

public class ServiceClassifier {

	final static String wsdlFolder = "/var/www/domains/travel";
	
	public void classify() {
		Scanner scanner = new Scanner(System.in);

        File folder = new File(wsdlFolder);
        File[] listOfFiles = folder.listFiles();

        System.out.println("The available WSDLs are:");
        for (int i = 0; i < listOfFiles.length; i++)
            if (listOfFiles[i].isFile()) {
                String file_name;
                file_name = listOfFiles[i].getName();
                System.out.println("    " + i + " - " + file_name);
            }
        
        System.out.println();
        System.out.print("Choose a WS: ");
        
        
        
        String serviceToEvaluate = listOfFiles[scanner.nextInt()].getAbsolutePath();
        WSDLReader readerService = new SAWSDLReader(serviceToEvaluate);
        
        System.out.println("WS to evaluate: ");
        System.out.println("name: " + readerService.getServiceName());
        System.out.println("inputs: " + readerService.getInputs());
        System.out.println("outputs: " + readerService.getOutputs());
        
        //evaluate each WS in the repository
        for (int i = 0; i < listOfFiles.length; i++)
            if (listOfFiles[i].isFile()) {
            	String serviceExaminee = listOfFiles[i].getAbsolutePath();
            	ServiceMatcher matcher = new ServiceMatcher(serviceToEvaluate, serviceExaminee);
            	double functionalSimilarity = matcher.getFunctionalSimilarity();
            	
            	if(functionalSimilarity != 0.0) {
            		WSDLReader examineeService = new SAWSDLReader(serviceExaminee);
            		System.out.println("*****************");
            		System.out.println("WS found: " + examineeService.getServiceName());
            		System.out.println("*****************");
                    System.out.println("name: " + examineeService.getServiceName());
                    System.out.println("inputs: " + examineeService.getInputs());
                    System.out.println("outputs: " + examineeService.getOutputs());
            		System.out.println("score: " + functionalSimilarity);
            	}
            }
        
	}
}
