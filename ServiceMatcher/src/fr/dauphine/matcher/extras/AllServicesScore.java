package fr.dauphine.matcher.extras;

import java.io.File;
import java.util.Scanner;

import org.jgrapht.util.PrefetchIterator.NextElementFunctor;

import fr.dauphine.matcher.ServiceMatcher;
import fr.dauphine.matcher.wsdl.SAWSDLReader;
import fr.dauphine.matcher.wsdl.WSDLReader;

public class AllServicesScore {

	final static String wsdlFolder = "/var/www/domains/travel";
	
	public void getAllScores() {
		Scanner scanner = new Scanner(System.in);

        File folder = new File(wsdlFolder);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
            	
            	String serviceToEvaluate = listOfFiles[i].getAbsolutePath();
		        WSDLReader readerService = new SAWSDLReader(serviceToEvaluate);
		        
		        System.out.println("*****************");
		        System.out.println("* Evaluated WS: *");
		        System.out.println("*****************");
		        System.out.println("name: " + readerService.getServiceName());
		        System.out.println("inputs: " + readerService.getInputs());
		        System.out.println("outputs: " + readerService.getOutputs());
		        
		        //evaluate each WS in the repository
		        for (int j = 0; j < listOfFiles.length; j++)
		            if (listOfFiles[j].isFile()) {
		            	String serviceExaminee = listOfFiles[j].getAbsolutePath();
		            	ServiceMatcher matcher = new ServiceMatcher(serviceToEvaluate, serviceExaminee);
		            	double functionalSimilarity = matcher.getFunctionalSimilarity();
		            	
		            	if(functionalSimilarity != 0.0) {
		            		WSDLReader examineeService = new SAWSDLReader(serviceExaminee);
		            		System.out.println("* Match: ");
		                    System.out.println("---name: " + examineeService.getServiceName());
		                    System.out.println("---inputs: " + examineeService.getInputs());
		                    System.out.println("---outputs: " + examineeService.getOutputs());
		            		System.out.println("---score: " + functionalSimilarity);
		            	}
		            }
            }
        }
        
	}
	public static void main(String...args) {
		AllServicesScore allScores = new AllServicesScore();
		allScores.getAllScores();
	}
}
